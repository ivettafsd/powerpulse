const express = require('express');
const ctrl = require('../../controllers/diary');
const { validateBody, authenticate } = require('../../middlewares');
const { schemas } = require('../../models/diary');
const router = express.Router();

router.post('/product/add', authenticate, validateBody(schemas.addEatenProduct), ctrl.addEatenProduct);
router.delete('/product/delete', authenticate, validateBody(schemas.removeEatenProduct), ctrl.removeEatenProduct);
router.post('/exercise/add', authenticate, validateBody(schemas.addDoneExercise), ctrl.addDoneExercise);
router.delete('/exercise/delete', authenticate, validateBody(schemas.removeDoneExercise), ctrl.removeDoneExercise);
router.get('/daily/:date', authenticate, ctrl.getDailyData);
router.get('/monthly/:date', authenticate, ctrl.getMonthlyData);

module.exports = router;
