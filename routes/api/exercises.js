const express = require('express');
const ctrl = require('../../controllers/exercises');
const { authenticate } = require('../../middlewares');

const router = express.Router();

router.get('/', authenticate, ctrl.getAllExercises);
router.get('/bodyparts', authenticate, ctrl.getAllBodyparts);
router.get('/equipments', authenticate, ctrl.getAllEquipments);
router.get('/muscles', authenticate, ctrl.getAllMuscles);

module.exports = router;
