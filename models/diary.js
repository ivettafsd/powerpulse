const { handleMangooseError, HttpError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const dateRegexp = /^(20[0-9]{2})-(0[1-9]|1[0-2])-([0-2]\d|3[01])$/;

const diarySchema = new Schema(
  {
    date: {
      type: String,
      match: dateRegexp,
      required: true,
    },
    consumedCalories: {
      type: Number,
      required: true,
      default: 0,
    },
    eatenProducts: {
      type: [
        {
          product: {
            type: String,
            ref: 'product',
          },
          amount: Number,
          calories: Number,
        },
      ],
      default: [],
      required: true,
    },
    burnedCalories: {
      type: Number,
      required: true,
      default: 0,
    },
    sportTime: {
      type: Number,
      required: true,
      default: 0,
    },
    doneExercises: {
      type: [
        {
          exercise: {
            type: String,
            ref: 'exercise',
          },
          time: Number,
          calories: Number,
        },
      ],
      default: [],
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

diarySchema.post('save', handleMangooseError);

const addEatenProduct = Joi.object({
  date: Joi.string()
    .required()
    .empty(false)
    .pattern(dateRegexp)
    .custom((value) => {
      const currentDate = Date.now();
      const date = new Date(value);
      if (date <= currentDate) {
        return value;
      } else {
        throw HttpError(400, 'Date must not be in the future');
      }
    })
    .messages({
      'string.base': 'The date must be a string.',
      'any.required': 'The date field is required.',
      'string.empty': 'The date must not be empty',
      'string.pattern.base': 'The date must be in the format "YYYY-MM-DD"',
    }),
  product: Joi.string().required().empty(false).messages({
    'string.base': 'The product must be a string.',
    'any.required': 'The product field is required.',
    'string.empty': 'The product must not be empty',
  }),
  amount: Joi.number().min(1).required().messages({
    'number.base': 'The amount must be a number.',
    'number.min': 'The amount must be at least 1.',
    'any.required': 'The amount field is required.',
  }),
  calories: Joi.number().min(1).required().messages({
    'number.base': 'The calories must be a number.',
    'number.min': 'The calories must be at least 1.',
    'any.required': 'The calories field is required.',
  }),
});

const removeEatenProduct = Joi.object({
  date: Joi.string()
    .required()
    .empty(false)
    .pattern(dateRegexp)
    .custom((value) => {
      const currentDate = Date.now();
      const date = new Date(value);
      if (date <= currentDate) {
        return value;
      } else {
        throw HttpError(400, 'Date must not be in the future');
      }
    })
    .messages({
      'string.base': 'The date must be a string.',
      'any.required': 'The date field is required.',
      'string.empty': 'The date must not be empty',
      'string.pattern.base': 'The date must be in the format "YYYY-MM-DD"',
    }),
  eatenProductId: Joi.string().required().empty(false).messages({
    'string.base': 'The eaten product id must be a string.',
    'any.required': 'The eaten product id field is required.',
    'string.empty': 'The eaten product id must not be empty',
  }),
});

const addDoneExercise = Joi.object({
  date: Joi.string()
    .required()
    .empty(false)
    .pattern(dateRegexp)
    .custom((value) => {
      const currentDate = Date.now();
      const date = new Date(value);
      if (date <= currentDate) {
        return value;
      } else {
        throw HttpError(400, 'Date must not be in the future');
      }
    })
    .messages({
      'string.base': 'The date must be a string.',
      'any.required': 'The date field is required.',
      'string.empty': 'The date must not be empty',
      'string.pattern.base': 'The date must be in the format "YYYY-MM-DD"',
    }),
  exercise: Joi.string().required().empty(false).messages({
    'string.base': 'The exercise id must be a string.',
    'any.required': 'The exercise id field is required.',
    'string.empty': 'The exercise id must not be empty',
  }),
  time: Joi.number().min(1).required().messages({
    'number.base': 'The time must be a number',
    'number.min': 'The  time must be at least 1.',
    'any.required': 'The time field is required',
  }),
  calories: Joi.number().min(1).required().messages({
    'number.base': 'The calories must be a number',
    'number.min': 'The  calories must be at least 1.',
    'any.required': 'The calories field is required',
  }),
});

const removeDoneExercise = Joi.object({
  date: Joi.string()
    .required()
    .empty(false)
    .pattern(dateRegexp)
    .custom((value) => {
      const currentDate = Date.now();
      const date = new Date(value);
      if (date <= currentDate) {
        return value;
      } else {
        throw HttpError(400, 'Date must not be in the future');
      }
    })
    .messages({
      'string.base': 'The date must be a string.',
      'any.required': 'The date field is required.',
      'string.empty': 'The date must not be empty',
      'string.pattern.base': 'The date must be in the format "YYYY-MM-DD"',
    }),
  doneExerciseId: Joi.string().required().empty(false).messages({
    'string.base': 'The done exercise id must be a string.',
    'any.required': 'The done exercise id field is required.',
    'string.empty': 'The done exercise id must not be empty',
  }),
});

const Diary = model('diary', diarySchema);
const schemas = { addEatenProduct, addDoneExercise, removeEatenProduct, removeDoneExercise };

module.exports = { Diary, schemas };
