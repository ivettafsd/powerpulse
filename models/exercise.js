const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');

const exerciseSchema = new Schema(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    bodyPart: {
      type: String,
      required: true,
    },
    equipment: {
      type: String,
      required: true,
    },
    gifUrl: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    target: {
      type: String,
      required: true,
    },
    time: {
      type: Number,
      required: false,
    },
    burnedCalories: {
      type: Number,
      required: false,
    },
  },
  { versionKey: false, timestamps: true },
);

exerciseSchema.post('save', handleMangooseError);

const Exercise = model('exercise', exerciseSchema);

module.exports = { Exercise };
