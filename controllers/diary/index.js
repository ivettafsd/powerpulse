const { CtrlWrapper } = require('../../helpers');
const addEatenProduct = require('./addEatenProduct');
const addDoneExercise = require('./addDoneExercise');
const getDailyData = require('./getDailyData');
const getMonthlyData = require('./getMonthlyData');
const removeDoneExercise = require('./removeDoneExercise');
const removeEatenProduct = require('./removeEatenProduct');

module.exports = {
  addEatenProduct: CtrlWrapper(addEatenProduct),
  addDoneExercise: CtrlWrapper(addDoneExercise),
  getDailyData: CtrlWrapper(getDailyData),
  getMonthlyData: CtrlWrapper(getMonthlyData),
  removeDoneExercise: CtrlWrapper(removeDoneExercise),
  removeEatenProduct: CtrlWrapper(removeEatenProduct),
};
