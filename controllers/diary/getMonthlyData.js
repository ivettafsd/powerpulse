const { Diary } = require('../../models/diary');
const { HttpError } = require('../../helpers');

const dateRegexp = /^(20[0-9]{2})-(0[1-9]|1[0-2])$/;

const getMonthlyData = async (req, res) => {
  const { _id } = req.user;
  const { date } = req.params;

  if (!dateRegexp.test(date)) {
    throw HttpError(400, 'Date invalid');
  }

  const data = await Diary.find({ date: { $regex: '^' + date }, user: _id })
    .sort({ date: 1 })
    .populate({
      path: 'user',
      select: 'bodyData.dailyRateCalories bodyData.Min', // Specify the fields you want to populate
    })
    .select('-createdAt -updatedAt');

  res.json(data);
};

module.exports = getMonthlyData;
