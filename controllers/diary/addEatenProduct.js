const { Diary } = require('../../models/diary');

const addEatenProduct = async (req, res) => {
  const { _id } = req.user;
  const { date, product, amount, calories } = req.body;

  const foundedDiary = await Diary.findOne({ date, user: _id });

  let dateDiary;
  if (!foundedDiary) {
    const newNote = {
      date,
      consumedCalories: calories,
      eatenProducts: [{ product: product.toString(), amount, calories }],
      user: _id,
    };

    dateDiary = await Diary.create(newNote);
  } else {
    dateDiary = await Diary.findOneAndUpdate(
      { _id: foundedDiary._id },
      {
        $inc: { consumedCalories: +calories },
        $push: { eatenProducts: { product: product.toString(), amount, calories } },
      },
      { new: true },
    );
  }
  res.json(dateDiary);
};

module.exports = addEatenProduct;
