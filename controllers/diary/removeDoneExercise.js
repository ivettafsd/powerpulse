const { Diary } = require('../../models/diary');
const { HttpError } = require('../../helpers');

const removeDoneExercise = async (req, res) => {
  const { _id } = req.user;
  const { date, doneExerciseId } = req.body;

  const foundedDiary = await Diary.findOne({ date, user: _id });
  
  const doneExercise = foundedDiary.doneExercises.find((exercise) => exercise._id.toString() === doneExerciseId);

  if (!foundedDiary || !doneExercise) {
    throw HttpError(401, 'Info about this date not found');
  } else {
    const data = await Diary.findByIdAndUpdate(
      foundedDiary._id,
      {
        $inc: { burnedCalories: -doneExercise.calories, sportTime: -time },
        $pull: { doneExercises: { _id: doneExerciseId } },
      },
      { new: true },
    ).select('-createdAt -updatedAt');
    res.json(data);
  }
};

module.exports = removeDoneExercise;
