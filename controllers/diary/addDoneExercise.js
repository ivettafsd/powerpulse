const { Diary } = require('../../models/diary');

const addDoneExercise = async (req, res) => {
  const { _id } = req.user;
  const { date, exercise, time, calories } = req.body;

  const foundedDiary = await Diary.findOne({ date, user: _id });

  let data;
  if (!foundedDiary) {
    const newNote = {
      date,
      burnedCalories: calories,
      sportTime: time,
      doneExercises: [{ exercise: exercise.toString(), time, calories }],
      user: _id,
    };

    data = await Diary.create(newNote);
  } else {
    data = await Diary.findByIdAndUpdate(
      foundedDiary._id,
      {
        $inc: { burnedCalories: +calories, sportTime: +time },
        $push: { doneExercises: { exercise: exercise.toString(), time, calories } },
      },
      { new: true },
    ).select('-createdAt -updatedAt');
  }
  res.json(data);
};

module.exports = addDoneExercise;
