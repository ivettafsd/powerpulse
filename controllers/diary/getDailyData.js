const { Exercise } = require('../../models/exercise');
const { Product } = require('../../models/product');
const { Diary } = require('../../models/diary');
const { HttpError } = require('../../helpers');

const dateRegexp = /^(20[0-9]{2})-(0[1-9]|1[0-2])-([0-2]\d|3[01])$/;

const getDailyData = async (req, res) => {
  const { _id } = req.user;
  const { date } = req.params;

  if (!dateRegexp.test(date)) {
    throw HttpError(400, 'Date invalid');
  }

  const data = await Diary.findOne({ date, user: _id })
    .populate({
      path: 'user',
      select: 'bodyData.dailyRateCalories bodyData.dailySportMin', // Specify the fields you want to populate for the 'user' field
    })
    .populate({
      path: 'eatenProducts.product',
      model: 'product',
    })
    .populate({
      path: 'doneExercises.exercise',
      model: 'exercise',
    })
    .select('-createdAt -updatedAt');

  res.json(data);
};

module.exports = getDailyData;
