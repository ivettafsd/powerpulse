const { Diary } = require('../../models/diary');
const { HttpError } = require('../../helpers');

const removeEatenProduct = async (req, res) => {
  const { _id } = req.user;
  const { date, eatenProductId } = req.body;

  const foundedDiary = await Diary.findOne({ date, user: _id });

  const eatenProduct = foundedDiary.eatenProducts.find((product) => product._id.toString() === eatenProductId);

  if (!foundedDiary || !eatenProduct) {
    throw HttpError(401, 'Info about this date not found');
  } else {
    const data = await Diary.findByIdAndUpdate(
      foundedDiary._id,
      {
        $inc: { consumedCalories: -eatenProduct.calories },
        $pull: { eatenProducts: { _id: eatenProductId } },
      },
      { new: true },
    ).select('-createdAt -updatedAt');
    res.json(data);
  }
};

module.exports = removeEatenProduct;
