const fs = require('fs/promises');
const path = require('path');

const bodypartsPath = path.join(__dirname, '../../data/bodyparts.json');

const getAllBodyparts = async (req, res) => {
  const bodyparts = await fs.readFile(bodypartsPath);
  res.json(JSON.parse(bodyparts));
};

module.exports = getAllBodyparts;
