const fs = require('fs/promises');
const path = require('path');

const equipmentsPath = path.join(__dirname, '../../data/equipments.json');

const getAllEquipments = async (req, res) => {
  const equipments = await fs.readFile(equipmentsPath);
  res.json(JSON.parse(equipments));
};

module.exports = getAllEquipments;
