const fs = require('fs/promises');
const path = require('path');

const musclesPath = path.join(__dirname, '../../data/muscles.json');

const getAllMuscles = async (req, res) => {
  const muscles = await fs.readFile(musclesPath);
  res.json(JSON.parse(muscles));
};

module.exports = getAllMuscles;
