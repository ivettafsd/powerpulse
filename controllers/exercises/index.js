const { CtrlWrapper } = require('../../helpers');
const getAllExercises = require('./getAllExercises');
const getAllBodyparts = require('./getAllBodyparts');
const getAllMuscles = require('./getAllMuscles');
const getAllEquipments = require('./getAllEquipments');

module.exports = {
  getAllExercises: CtrlWrapper(getAllExercises),
  getAllBodyparts: CtrlWrapper(getAllBodyparts),
  getAllMuscles: CtrlWrapper(getAllMuscles),
  getAllEquipments: CtrlWrapper(getAllEquipments),
};
