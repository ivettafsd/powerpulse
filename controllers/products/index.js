const { CtrlWrapper } = require('../../helpers');
const getProducts = require('./getProducts');
const getCategories = require('./getCategories');

module.exports = {
  getProducts: CtrlWrapper(getProducts),
  getCategories: CtrlWrapper(getCategories),
};
