const fs = require('fs/promises');
const path = require('path');

const categoriesPath = path.join(__dirname, '../../data/categories.json');

const getCategories = async (req, res) => {
  const categories = await fs.readFile(categoriesPath);
 res.json(JSON.parse(categories));
};

module.exports = getCategories;
