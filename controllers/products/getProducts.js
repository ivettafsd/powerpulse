const { Product } = require('../../models/product');
const { HttpError } = require('../../helpers');

const getProducts = async (req, res) => {
  const { bodyData } = req.user;
  const { category, isNotAllowed, title } = req.query;

  if (!bodyData) {
    throw HttpError(409, 'User body data is absent');
  }
  const query = {};
  category && (query.category = category);
  isNotAllowed !== undefined && (query[`groupBloodNotAllowed.${bodyData.blood}`] = isNotAllowed);
  title && (query.title = { $regex: title, $options: 'i' });

  const results = await Product.find(query);
  const updatedResults = results.map(({ _doc: { groupBloodNotAllowed, ...result } }) => {
    return { ...result, isNotAllowed: groupBloodNotAllowed[bodyData.blood] };
  });

  res.json(updatedResults);
};

module.exports = getProducts;
