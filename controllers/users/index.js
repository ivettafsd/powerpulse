const {  CtrlWrapper } = require('../../helpers');
const signup = require('./signup');
const signin = require('./signin');
const currentUser = require('./currentUser');
const updateUser = require('./updateUser');
const addBodyData = require('./addBodyData');
const signout = require('./signout');

module.exports = {
  signup: CtrlWrapper(signup),
  signin: CtrlWrapper(signin),
  currentUser: CtrlWrapper(currentUser),
  updateUser: CtrlWrapper(updateUser),
  addBodyData: CtrlWrapper(addBodyData),
  signout: CtrlWrapper(signout),
};
